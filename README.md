# Odin Kittens

This project is part of the Odin Project's Ruby on Rails course. The purpose of this project is to build a simple app which can be accessed via an API. It is just a warm up to get familiar with rendering JSON instead of HTML.
